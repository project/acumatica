<?php

declare(strict_types=1);

namespace Drupal\acumatica\Command;

use KrystalCode\Acumatica\Api\Session\SessionStorageInterface;
use Drush\Commands\DrushCommands;

/**
 * Commands related to session management.
 */
class Session extends DrushCommands {

  /**
   * Constructs a new Session object.
   *
   * @param \KrystalCode\Acumatica\Api\Session\SessionStorageInterface $sessionStorage
   *   The session storage.
   */
  public function __construct(
    protected SessionStorageInterface $sessionStorage
  ) {
  }

  /**
   * Deletes the session with the given ID.
   *
   * @param string|null $id
   *   The ID of the session, or NULL to delete the session with the default ID.
   *
   * @usage drush acumatica:delete-session "default"
   *   Delete the session with ID "default".
   *
   * @command acumatica:delete-session
   */
  public function deleteSession(string $id = NULL) {
    if ($id) {
      $this->sessionStorage->delete($id);
      return;
    }

    $this->sessionStorage->delete();
  }

}
