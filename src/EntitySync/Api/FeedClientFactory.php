<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\Api;

use Drupal\entity_sync\Client\ProviderClientFactoryInterface;
use Drupal\entity_sync\Exception\InvalidConfigurationException;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Factory for generating Acumatica OData feeds API clients.
 *
 * Supported parameters:
 * - connection_type: (string, required) The ID of the connection type from
 *   which to get the Acumatica environment that will be queried and the
 *   authentication credentials.
 * - object_type: (string, required) The Acumatica object type for which to
 *   generate the client. Note that it needs to be the same as configured in the
 *   feed URL path, that might or might not be the same as the general object
 *   type (class) in Acumatica.
 * - odata: (array, optional) An array containing `select` and `expand` items to
 *   add to the corresponding OData clauses. OData filters are not supported
 *   yet.
 */
class FeedClientFactory implements ProviderClientFactoryInterface {

  /**
   * Constructs a new FeedClientFactory object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\acumatica\EntitySync\Api\FeedClient
   *   The client for fetching the OData feed.
   */
  public function get($sync_id, array $parameters) {
    $required_parameters = [
      'connection_type',
      'object_type',
    ];
    foreach ($required_parameters as $required_parameter) {
      if (!in_array($required_parameter, array_keys($parameters))) {
        throw new \InvalidArgumentException(sprintf(
          'Parameter "%s" is required for creating the feed client.',
          $required_parameter
        ));
      }
    }

    return new FeedClient(
      $this->getAcumaticaConnection($parameters['connection_type']),
      $parameters['object_type'],
      $parameters['odata'] ?? []
    );
  }

  /**
   * Returns the details for connecting to Acumatica.
   *
   * We are eventually going to migrate to using the `entity_sync_session`
   * module and use its session types for defining connection and authentication
   * details. We therefore do not want to make the configuration object schema
   * more complex for the OData feeds. Let's just assume the currently supported
   * structure that provides the details for the API integrations and extract
   * the details we need for feeds from them.
   *
   * @param string $connection_type_id
   *   The ID of the Acumatica connection type configuration object.
   *
   * @return array
   *   An associative array containing the connection and authentication details
   *   required for fetching Acumatica OData feeds. For supported array items
   *   see \Drupal\acumatica\EntitySync\Api::__construct().
   */
  protected function getAcumaticaConnection(
    string $connection_type_id
  ): array {
    $connection_type = $this->entityTypeManager
      ->getStorage('acumatica_connection_type')
      ->load($connection_type_id);
    if ($connection_type === NULL) {
      throw new InvalidConfigurationException(sprintf(
        'Unknown Acumatica connection type "%s".',
        $connection_type_id
      ));
    }

    $authentication = $connection_type->get('authentication');

    return [
      'base_uri' => explode('identity', $authentication['authorize_url'])[0],
      'environment' => explode('@', $authentication['client_id'])[1],
      'username' => $authentication['username'],
      'password' => $authentication['password'],
    ];
  }

}
