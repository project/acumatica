<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\Api;

use Drupal\entity_sync\Client\ClientInterface;

use GuzzleHttp\Client;
use KrystalCode\ApiIterator\ClientInterface as IteratorClientInterface;
use KrystalCode\ApiIterator\Iterator as ApiIterator;

/**
 * The API client Entity Sync adapter for the Acumatica OData feeds.
 */
class FeedClient implements ClientInterface, IteratorClientInterface {

  use ApiIteratorClientTrait;
  use OdataClientTrait;

  /**
   * Constructs a new FeedClient object.
   *
   * @param array $connection
   *   An associative array containing the connection and authentication details
   *   required for fetching Acumatica OData feeds.
   *   - base_uri (string, required): The URL of the Acumatica instance.
   *   - environment (string, required): The ID of the Acumatica environment.
   *   - username (string, required): The username that will be used for basic
   *     HTTP authentication.
   *   - password (string, required): The password that will be used for basic
   *     HTTP authentication.
   * @param string $objectType
   *   The object type (resource) for which we'll be fetching the feed e.g.
   *   `StockItem`. Note that it needs to be the same as configured in the feed
   *   URL path, that might or might not be the same as the general object type
   *   (class) in Acumatica.
   * @param array $oData
   *   An array containing `select` and `expand` items to add to the
   *   corresponding OData clauses. OData filters are not supported yet.
   */
  public function __construct(
    protected array $connection,
    protected string $objectType,
    protected array $oData = []
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * phpcs:disable
   * @I Document all supported client options
   *    type     : task
   *    priority : normal
   *    labels   : documentation
   * phpcs:enable
   */
  public function importList(array $filters = [], array $options = []) {
    if (!isset($options['odata'])) {
      $options['odata'] = $this->oData;
    }

    return $this->list(
      $this->buildListRequestOptions($filters, $options),
      [
        'select' => $this->buildOdataOption('select', $options['odata']),
        'filter' => $this->buildOdataFilter($filters, $options['odata'], FALSE),
        'expand' => $this->buildOdataOption('expand', $options['odata']),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function importEntity($id) {
    throw new \Exception('Importing individual entities using OData Feeds is not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $fields, array $options = []) {
    throw new \Exception('Exporting individual entities using OData Feeds is not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function update($id, array $fields, array $options = []) {
    throw new \Exception('Exporting individual entities using OData Feeds is not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function list(array $options = [], array $query = []) {
    $options = array_merge(
      [
        'page' => 1,
        'limit' => 100,
        'delay' => NULL,
      ],
      $options
    );
    $query = array_merge(
      [
        'select' => NULL,
        'filter' => NULL,
        'expand' => NULL,
        'custom' => NULL,
      ],
      $query
    );
    $query['skip'] = ($options['page'] - 1) * $options['limit'];
    $query['top'] = $options['limit'];

    // By default, we return an iterator. Caller code can then loop over it
    // and the iterator will internally handle making the API calls for
    // fetching the objects for each page.
    if (empty($options['bypass_iterator'])) {
      return new ApiIterator(
        $this,
        $options['page'],
        $options['limit'],
        $query,
        FALSE,
        $options['delay']
      );
    }

    // Otherwise, either it is requested to fetch the results directly, or
    // this is a call by the iterator while being looped over. Make the
    // request and return the results.
    unset($options['bypass_iterator']);

    $objects = $this->getFeed($query);

    $has_more = NULL;
    $count = count($objects);
    if ($count === 0 || $count < $options['limit']) {
      $has_more = FALSE;
    }

    return [
      new \CachingIterator(
        new \ArrayIterator($objects),
        \CachingIterator::FULL_CACHE
      ),
      $has_more,
      $query,
    ];
  }

  /**
   * Fetches and returns the objects from the feed.
   *
   * @param array $query
   *   The query parameters for fetching the feed.
   *
   * @return array
   *   The objects.
   */
  protected function getFeed(array $query) {
    $relative_uri = "OData/{$this->connection['environment']}/{$this->objectType}?\$format=json";

    foreach ($query as $name => $value) {
      if ($value === NULL) {
        continue;
      }

      $relative_uri .= "&\${$name}={$value}";
    }

    $client = new Client(
      ['base_uri' => $this->connection['base_uri']]
    );
    $response = $client->request(
      'GET',
      $relative_uri,
      ['auth' => [$this->connection['username'], $this->connection['password']]]
    );

    $response = json_decode((string) $response->getBody());

    if (!property_exists($response, 'value')) {
      throw new \RuntimeException('No value included in the Acumatica OData feed response.');
    }

    return $response->value;
  }

}
