<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\Api;

/**
 * Provides methods for Entity Synchronization clients that use API iterators.
 */
trait ApiIteratorClientTrait {

  /**
   * Builds and returns the options for the API iterator client.
   *
   * @param array $filters
   *   The filters passed to the client.
   * @param array $options
   *   The options passed to the client.
   *
   * @return array
   *   The options array to be passed to the list request.
   */
  protected function buildListRequestOptions(
    array $filters,
    array $options
  ): array {
    // Support delay between requests that can reduce the risk of hitting
    // Acumatica's API rate and processing limits. If not given, no delay will
    // be applied.
    $request_options = [
      'delay' => $options['delay'] ?? NULL,
    ];

    // Number of items per page. If not given, the iterator client's `list`
    // method will apply the default of 100.
    if (isset($options['limit'])) {
      $request_options['limit'] = $options['limit'];
    }
    // Page to start at; we may be continuing a state-managed import that did
    // not finish all pages in its last run.
    // If not given, the iterator client's `list` method will apply the default
    // of 1 i.e. start at the first page.
    if (!empty($filters['offset']) && isset($request_options['limit'])) {
      $request_options['page'] = ((int) floor($filters['offset'] / $request_options['limit'])) + 1;
    }

    return $request_options;
  }

}
