<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\Api;

use Drupal\acumatica\Session\OAuth2\PasswordSessionManager;
use Drupal\entity_sync\Client\ProviderClientFactoryInterface;
use Drupal\entity_sync\Exception\InvalidConfigurationException;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

use KrystalCode\Acumatica\Api\Configuration;
use KrystalCode\Acumatica\Api\Session\SessionStorageInterface;

/**
 * Factory for generating Acumatica object API clients.
 *
 * Supported parameters:
 * - connection_type: (string, required) The ID of the connection type to use.
 * - object_type: (string, required) The Acumatica object type for which to
 *   generate the client.
 * - session_duration: (int, optional) The minimum session duration to pass to
 *   the session manager, in seconds.
 * - odata: (array, optional) An array containing `select` and `expand` items to
 *   add to the corresponding OData clauses. Currenlty only supported by the
 *   `importList` client method. OData filters are not supported yet.
 */
class ObjectClientFactory implements ProviderClientFactoryInterface {

  /**
   * Constructs a new ObjectClientFactory object.
   *
   * @param \KrystalCode\Acumatica\Api\Session\SessionStorageInterface $sessionStorage
   *   The session storage.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Drupal system time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The session storage.
   */
  public function __construct(
    protected SessionStorageInterface $sessionStorage,
    protected TimeInterface $time,
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function get($sync_id, array $parameters) {
    $required_parameters = [
      'connection_type',
      'object_type',
    ];
    foreach ($required_parameters as $required_parameter) {
      if (!in_array($required_parameter, array_keys($parameters))) {
        throw new \InvalidArgumentException(sprintf(
          'Parameter "%s" is required for creating the object client.',
          $required_parameter
        ));
      }
    }

    $session_options = [];
    if (isset($parameters['session_duration'])) {
      $session_options['session_duration'] = $parameters['session_duration'];
    }

    // phpcs:disable
    // @I Allow injecting the session manager
    //    type     : task
    //    priority : low
    //    labels   : modularity, testing
    //    notes    : For mocking purposes and for supporting other session
    //               managers.
    // phpcs:enable
    return new ObjectClient(
      new PasswordSessionManager(
        $this->sessionStorage,
        $this->time,
        $this->getAcumaticaConfiguration($parameters['connection_type']),
        $session_options
      ),
      $parameters['object_type'],
      $parameters['odata'] ?? []
    );
  }

  /**
   * Instantiates and returns the Acumatica API configuration.
   *
   * @param string $connection_type_id
   *   The ID of the connection type to use for building the Acumatica
   *   configuration.
   *
   * @return \KrystalCode\Acumatica\Api\Configuration
   *   The Acumatica API configuration.
   */
  protected function getAcumaticaConfiguration(
    string $connection_type_id
  ): Configuration {
    $connection_type = $this->entityTypeManager
      ->getStorage('acumatica_connection_type')
      ->load($connection_type_id);
    if ($connection_type === NULL) {
      throw new InvalidConfigurationException(sprintf(
        'Unknown Acumatica connection type "%s".',
        $connection_type_id
      ));
    }

    return new Configuration(
      $connection_type->getApiId(),
      $connection_type->getApiVersion(),
      $connection_type->getBaseNamespace(),
      $connection_type->getAuthentication()
    );
  }

}
