<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\Api;

use Drupal\entity_sync\Client\ClientInterface;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use KrystalCode\Acumatica\Api\Discovery\Discovery;
use KrystalCode\Acumatica\Api\Session\SessionManagerInterface;
use Psr\Http\Message\RequestInterface;

/**
 * The API client Entity Sync adapter for the Acumatica object APIs.
 */
class ObjectClient implements ClientInterface, ObjectClientInterface {

  use ApiIteratorClientTrait;
  use OdataClientTrait;

  /**
   * Constructs a new ObjectClient object.
   *
   * @param \KrystalCode\Acumatica\Api\Session\SessionManagerInterface $sessionManager
   *   The configuration factory.
   * @param string $objectType
   *   The object type (resource) of the API client that will be used
   *   e.g. `StockItem`.
   * @param array $oData
   *   An array containing `select` and `expand` items to add to the
   *   corresponding OData clauses. Currenlty only supported by the
   *   `importList` client method. OData filters are not supported yet.
   */
  public function __construct(
    protected SessionManagerInterface $sessionManager,
    protected string $objectType,
    protected array $oData = []
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * phpcs:disable
   * @I Document all supported client options
   *    type     : task
   *    priority : normal
   *    labels   : documentation
   * phpcs:enable
   */
  public function importList(array $filters = [], array $options = []) {
    if (!isset($options['odata'])) {
      $options['odata'] = $this->oData;
    }
    if (isset($options['session_duration'])) {
      $this->sessionManager->setOptions([
        'session_duration' => $options['session_duration'],
      ]);
    }

    $discovery = new Discovery(
      $this->getGuzzleClient(
        __METHOD__,
        [
          'filters' => $filters,
          'options' => $options,
        ]
      ),
      $this->sessionManager
    );
    $api = lcfirst($this->objectType);
    return $discovery
      ->{$api}()
      ->list(
        $this->buildListRequestOptions($filters, $options),
        [
          'select' => $this->buildOdataOption('select', $options['odata']),
          'filter' => $this->buildOdataFilter($filters, $options),
          'expand' => $this->buildOdataOption('expand', $options['odata']),
        ]
      );
  }

  /**
   * {@inheritdoc}
   *
   * phpcs:disable
   * @I Add option that will instruct us to request attributes/properties
   *    type     : improvement
   *    priority : high
   *    labels   : client
   *    notes    : How do we get client options/query conditions here? Add an
   *               options argument to the interface method?
   * phpcs:enable
   */
  public function importEntity($id) {
    $discovery = new Discovery(new Client(), $this->sessionManager);
    $api = lcfirst($this->objectType);
    return $discovery
      ->{$api}()
      ->getById($id)
      ->jsonSerialize();
  }

  /**
   * {@inheritdoc}
   *
   * Support client options are:
   * - branch (array, optional): An associative array that instructs the client
   *   to include the branch in the request headers. Supported array elements
   *   are:
   *   - mode (string, required): The calculation mode. Supported modes are
   *     `static` and `field_mapping`. The `static` mode will use the provided
   *     fixed value as the branch. The `field_mapping` mode will use one of the
   *     field values that were transformed as defined by the field mapping of
   *     the synchronization.
   *   - static (array, optional): Required if the calculation mode is `static`.
   *     An associative array containing a single element, keyed by `value` and
   *     containing the branch to use.
   *   - field_mapping (array, optional): Required if the calculation mode is
   *     `field_mapping`. An associative array containing a single element,
   *     keyed by `remote_name` and containing the remote field name to use as
   *     the branch. This should normally match the same property defined in the
   *     field mapping of the synchronization.
   *
   * @link https://help-2022r2.acumatica.com/(W(82))/Help?ScreenId=ShowWiki&pageid=9821cff9-4970-4153-a0f8-dbf5758133a7
   */
  public function create(array $fields, array $options = []) {
    // Handle attributes.
    $fields = $this->buildAttributesField($fields);
    // Handle branch, if requested in options.
    [$fields, $branch] = $this->getBranch($fields, $options);

    $namespace = $this->sessionManager
      ->getConfiguration()
      ->getClientNamespace();
    $class = $namespace . '\\Model\\' . $this->objectType;
    $object = new $class($fields);

    $discovery = new Discovery(
      $this->getGuzzleClient(
        __METHOD__,
        [
          'branch' => $branch,
          'options' => $options,
        ]
      ),
      $this->sessionManager
    );
    $api = lcfirst($this->objectType);
    return $discovery
      ->{$api}()
      ->putEntity($object)
      ->jsonSerialize();
  }

  /**
   * {@inheritdoc}
   *
   * Support client options are:
   * - branch (array, optional): An associative array that instructs the client
   *   to include the branch in the request headers. Supported array elements
   *   are:
   *   - mode (string, required): The calculation mode. Supported modes are
   *     `static` and `field_mapping`. The `static` mode will use the provided
   *     fixed value as the branch. The `field_mapping` mode will use one of the
   *     field values that were transformed as defined by the field mapping of
   *     the synchronization.
   *   - static (array, optional): Required if the calculation mode is `static`.
   *     An associative array containing a single element, keyed by `value` and
   *     containing the branch to use.
   *   - field_mapping (array, optional): Required if the calculation mode is
   *     `field_mapping`. An associative array containing a single element,
   *     keyed by `remote_name` and containing the remote field name to use as
   *     the branch. This should normally match the same property defined in the
   *     field mapping of the synchronization.
   *
   * According to documentation, setting the current branch should apply to
   * update operations as well. Some testing on orders though worked only for
   * create operations i.e. orders remained on the branch after update requests.
   *
   * @link https://help-2022r2.acumatica.com/(W(82))/Help?ScreenId=ShowWiki&pageid=9821cff9-4970-4153-a0f8-dbf5758133a7
   */
  public function update($id, array $fields, array $options = []) {
    // Handle attributes.
    $fields = $this->buildAttributesField($fields);
    // Handle branch, if requested in options.
    [$fields, $branch] = $this->getBranch($fields, $options);

    $namespace = $this->sessionManager
      ->getConfiguration()
      ->getClientNamespace();
    $class = $namespace . '\\Model\\' . $this->objectType;
    $object = new $class($fields);
    $object->setId($id);

    $discovery = new Discovery(
      $this->getGuzzleClient(
        __METHOD__,
        [
          'branch' => $branch,
          'options' => $options,
        ]
      ),
      $this->sessionManager
    );
    $api = lcfirst($this->objectType);
    return $discovery
      ->{$api}()
      ->putEntity($object)
      ->jsonSerialize();
  }

  /**
   * {@inheritdoc}
   */
  public function getByKeys(array $key_values): object {
    $discovery = new Discovery(new Client(), $this->sessionManager);
    $api = lcfirst($this->objectType);
    return $discovery
      ->{$api}()
      ->getByKeys($key_values)
      ->jsonSerialize();
  }

  /**
   * Collects all attributes in a single field for exports.
   *
   * We commonly have a Drupal field to an Acumatica attribute, and the most
   * convenient/common way is to have a field mapping for each field/attribute
   * combination. However, in Acumatica API objects all attributes are stored
   * together as an object in the `Attributes` field. We therefore need to
   * combine all attributes in one field.
   * The convention is that if the remote name on the field mapping is
   * `Attributes/{$MyAttribute}` then the transformed value being exported is
   * added to the `Attributes` field as the `{$MyAttribute}` attribute.
   *
   * @param array $fields
   *   The fields as passed to the `create` or `update` methods.
   *
   * @return array
   *   An associative array containing all fields as passed in the `$fields`
   *   argument, with all attributes moved from individual array elements to one
   *   element keyed `attributes`.
   */
  protected function buildAttributesField(array $fields): array {
    $attributes = [];

    foreach ($fields as $name => $value) {
      $name_parts = explode('/', $name);
      if (count($name_parts) !== 2) {
        continue;
      }
      if ($name_parts[0] !== 'Attributes') {
        continue;
      }

      $attributes[] = [
        'AttributeID' => ['value' => $name_parts[1]],
        'Value' => $value,
      ];
      unset($fields[$name]);
    }

    if ($attributes) {
      $fields['attributes'] = $attributes;
    }

    return $fields;
  }

  /**
   * Calculates and returns the Acumatica branch based on the given options.
   *
   * This currently applies only create/update export operations.
   *
   * If the branch calculation mode is based on field mapping, the value will be
   * taken from the transformed field values. The corresponding field value will
   * be removed from the array of the transformed field values that is
   * returned. This is because the branch is not needed to be sent in the
   * fields, we just need the header.
   *
   * @param array $fields
   *   The associative array containing the transformed field values.
   * @param array $options
   *   The client options.
   *
   * @return array
   *   A numerical array containing the updated field array as its first
   *   element, and the Acumatica branch as its second element (string, or NULL
   *   if no branch was detected).
   *
   * @throws \RuntimeException
   *   If the calculation mode relies on a field that was not found in the
   *   transformed fields.
   */
  protected function getBranch(
    array $fields,
    array $options
  ): array {
    if (!isset($options['branch'])) {
      return [$fields, NULL];
    }

    $this->validateBranchOptions($options['branch']);

    if ($options['branch']['mode'] === 'static') {
      return [$fields, $options['branch']['static']['value']];
    }

    // Field mapping it is otherwise - see validation.
    $field_name = $options['branch']['field_mapping']['remote_name'];
    if (!isset($fields[$field_name])) {
      throw new \RuntimeException(sprintf(
        'Field "%s" not found in the calculated field values when executing branch detection.',
        $field_name
      ));
    }
    $branch = $fields[$field_name];
    unset($fields[$field_name]);
    return [$fields, $branch];
  }

  /**
   * Validates the given branch options.
   *
   * @param array $options
   *   The associative array containing the branch property of the client
   *   options.
   *
   * @throws \InvalidArgumentException
   *   When a property has an invalid value or when a required property is
   *   missing.
   */
  protected function validateBranchOptions(array $options): void {
    if (!isset($options['mode'])) {
      throw new \InvalidArgumentException(
        'The branch detection mode must be configured.'
      );
    }

    switch ($options['mode']) {
      case 'field_mapping':
        if (!isset($options['field_mapping']['remote_name'])) {
          throw new \InvalidArgumentException(
            'The remote name of the field mapping item that provides the branch must be provided for field mapping-based branch detection.'
          );
        }
        break;

      case 'static':
        if (!isset($options['static']['value'])) {
          throw new \InvalidArgumentException(
            'A value must be provided for static branch detection.'
          );
        }
        break;

      default:
        throw new \InvalidArgumentException(sprintf(
          'Unknown branch detection mode "%s".',
          $options['mode']
        ));
    }
  }

  /**
   * Returns the instantiated Guzzle client for making requests.
   *
   * Currently, it adds the appropriate header if a branch is given.
   *
   * @param string $caller
   *   The name of the method that requested the client e.g. `create`, `update`
   *   etc.
   * @param array $options
   *   An array of options that may be used to determine the middleware that
   *   will be added to the client handler. This is not the same as, but may
   *   contain, the client options passed to the caller method. Supported
   *   options by the default implementation are:
   *   - branch (string, optional):
   *     The Acumatica branch to use for the header, or NULL to not include the
   *     branch header.
   *
   * @return \GuzzleHttp\Client
   *   The instantiated Guzzle client.
   */
  protected function getGuzzleClient(
    string $caller,
    array $options = []
  ): Client {
    if (!isset($options['branch'])) {
      return new Client();
    }

    // We use a middleware so that all requests sent by this client will have
    // the header.
    $branch = $options['branch'];
    $handler = HandlerStack::create();
    $handler->push(Middleware::mapRequest(
      function (RequestInterface $request) use ($branch) {
        return $request->withHeader('PX-CbApiBranch', $branch);
      })
    );

    // phpcs:disable
    // @I Allow injecting the Guzzle client for mocking or config purposes
    //    type     : task
    //    priority : low
    //    labels   : testing
    // phpcs:enable
    return new Client(['handler' => $handler]);
  }

}
