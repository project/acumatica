<?php

namespace Drupal\acumatica\EntitySync\Api;

/**
 * Provides the interface for Acumatica object clients.
 */
interface ObjectClientInterface {

  /**
   * Retrieves a record by its key fields.
   *
   * @param array $key_values
   *   An array containing the values for the key fields of the object being
   *   retrieved in the order required to be passed in the URL.
   *
   * @link https://help.acumatica.com/(W(73))/Help?ScreenId=ShowWiki&pageid=52c97a83-1fa1-40e9-8219-52a89a91f2da
   */
  public function getByKeys(array $key_values): object;

}
