<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\Api;

use KrystalCode\Acumatica\Api\OData\Filter;
use KrystalCode\Acumatica\Api\OData\FilterGroup;

/**
 * Provides OData-related methods for Entity Synchronization clients.
 */
trait OdataClientTrait {

  /**
   * Builds and returns a `select` or `expand` OData v3.0 query option.
   *
   * These are simpler than filters and there's not much to build. Each clause
   * is a comma-separated list of items. We add the comma separators here so
   * that the items can be passed as an array.
   *
   * @param string $option_id
   *   The identifier of the option i.e. `select` or `expand`.
   * @param array $options
   *   The oData options passed to the client.
   *
   * @return string|null
   *   The option formatted as expected by OData v3.0 URL conventions, or NULL
   *   if no option was defined.
   */
  protected function buildOdataOption(
    string $option_id,
    array $options
  ): ?string {
    if (!isset($options[$option_id])) {
      return NULL;
    }

    return implode(',', $options[$option_id]);
  }

  /**
   * Builds and returns the filter query option as expected by OData v3.
   *
   * @param array $filters
   *   The filters passed to the client.
   * @param array $options
   *   The options passed to the client. For backwards compatibility, only the
   *   `odata` array item can also be passed.
   * @param bool $offset
   *   `TRUE` to build the time filters with 'datetimeoffset', `FALSE` for
   *   'datetime'. Defaults to `TRUE` for backwards compatibility.
   *
   * @return string|null
   *   The option formatted as expected by OData v3.0 URL conventions, or NULL
   *   if no filter was defined.
   * phpcs:disable
   * @I Support multiple filter groups/operators
   *    type     : improvement
   *    priority : normal
   *    labels   : client, odata
   * phpcs:enable
   */
  protected function buildOdataFilter(
    array $filters,
    array $options,
    bool $offset = TRUE
  ): ?string {
    $odata_options = $options['odata'] ?? $options;
    $changed_field = $options['changed_field'] ?? 'LastModified';

    // Currently we only support one filter group with the `and` operator. We
    // need to study the OData protocol a bit more before we can add proper
    // support for more groups/operators.
    $odata_filter = new FilterGroup('and');

    // Time filters.
    if (isset($filters['changed_start'])) {
      $odata_filter->add(new Filter(
        $changed_field,
        $this->timestampToOdataDateTime($filters['changed_start'], $offset),
        'ge'
      ));
    }
    if (isset($filters['changed_end'])) {
      $odata_filter->add(new Filter(
        $changed_field,
        $this->timestampToOdataDateTime($filters['changed_end'], $offset),
        'le'
      ));
    }

    // Add other filters.
    if (isset($odata_options['filter'])) {
      foreach ($odata_options['filter'] as $filter) {
        $odata_filter->add(new Filter(
          $filter['key'],
          $filter['value'],
          $filter['operator']
        ));
      }
    }

    $formatted_odata_filter = (string) $odata_filter;
    if ($formatted_odata_filter === '') {
      return NULL;
    }

    return $formatted_odata_filter;
  }

  /**
   * Converts a Unix timestamp to an ISO8601 time OData URL parameter value.
   *
   * @param int $timestamp
   *   The Unix timestamp to convert.
   * @param bool $offset
   *   `TRUE` to return as 'datetimeoffset', `FALSE` for 'datetime'.
   *
   * @return string
   *   The time in the format expected by OData v3.0 URL conventions for
   *   datetime/datetime offset parameter values.
   */
  protected function timestampToOdataDateTime(
    int $timestamp,
    bool $offset = TRUE
  ): string {
    $datetime = new \DateTime('now', new \DateTimeZone('UTC'));
    $datetime->setTimestamp($timestamp);

    $iso8601 = $datetime->format('Y-m-d\TH:i:s') . ".000";
    $prefix = $offset ? 'datetimeoffset' : 'datetime';

    return "{$prefix}'{$iso8601}'";
  }

}
