<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\Runner;

use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Import\ManagerInterface as ImportManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Runner for operation configurator plugins that operate on lists of entities.
 *
 * Currently only supports importing entities. Support for exporting entities
 * will be added here.
 */
class EntityList extends Base {

  /**
   * Constructs a new EntityList object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger channel.
   * @param \Drupal\entity_sync\Import\ManagerInterface $importManager
   *   The Entity Synchronization entity import manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LoggerInterface $logger,
    protected ImportManagerInterface $importManager
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * Supported context options are:
   * - filters (array, optional) An array containing the filters to pass to the
   *   import manager.
   * - options (array) An associative array with options to pass to the import
   *   manager.
   *
   * @see \Drupal\entity_sync\Import\ManagerInterface::importRemoteList()
   */
  protected function doRun(
    OperationInterface $operation,
    array $context
  ): void {
    $this->importManager->importRemoteList(
      $operation->bundle(),
      $context['filters'] ?? [],
      $context['options'] ?? []
    );
  }

}
