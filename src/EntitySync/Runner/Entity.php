<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\Runner;

use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Export\EntityManagerInterface as ExportManagerInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Runner for operation configurator plugins that operate on single entities.
 *
 * Currently only supports exporting individual entities. Support for importing
 * individual entities will be added here.
 */
class Entity extends Base {

  /**
   * Constructs a new Entity object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger channel.
   * @param \Drupal\entity_sync\Export\EntityManagerInterface $exportManager
   *   The Entity Synchronization entity export manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LoggerInterface $logger,
    protected ExportManagerInterface $exportManager
  ) {
    parent::__construct($this->entityTypeManager, $this->logger);
  }

  /**
   * {@inheritdoc}
   *
   * Supported context options are:
   * - options (array) An associative array with options to pass to the export
   *   manager. The operation entity is passed in the context automatically.
   *
   * @see \Drupal\entity_sync\Export\ManagerInterface::exportLocalEntity()
   */
  protected function doRun(
    OperationInterface $operation,
    array $context
  ): void {
    $options = $context['options'] ?? [];
    if (!isset($options['context'])) {
      $options['context'] = [];
    }
    $options['context']['operation_entity'] = $operation;

    $this->exportManager->exportLocalEntity(
      $operation->bundle(),
      $operation->get(OperationField::ENTITY)->entity,
      $options
    );
  }

}
