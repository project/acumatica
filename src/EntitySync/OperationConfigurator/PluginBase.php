<?php

declare(strict_types=1);

namespace Drupal\acumatica\EntitySync\OperationConfigurator;

use Drupal\entity_sync\Entity\Runner\RunnerInterface;
use Drupal\entity_sync\OperationConfigurator\PluginBase as EntitySyncPluginBase;
use Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for operation configurator plugins.
 */
abstract class PluginBase extends EntitySyncPluginBase implements
  ContainerFactoryPluginInterface,
  SupportsRunnerInterface {

  /**
   * The ID of the operation runner service.
   *
   * Using the ID of a non-existing service forces child classes to define the
   * runner service. We cannot do it via an abstract method because we use it in
   * the `create` method that is static.
   */
  const RUNNER_SERVICE_ID = 'non-existing-service';

  /**
   * The runner for operations the types of which are configured by the plugin.
   *
   * @var \Drupal\entity_sync\Entity\Runner\RunnerInterface
   */
  protected $runner;

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\entity_sync\Entity\Runner\RunnerInterface $runner
   *   The operation runner.
   * phpcs:disable
   * @I The runner should be lazy injected
   *    type     : improvement
   *    priority : normal
   *    labels   : performance
   * phpcs:enable
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RunnerInterface $runner
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->runner = $runner;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get(static::RUNNER_SERVICE_ID)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runner() {
    return $this->runner;
  }

}
