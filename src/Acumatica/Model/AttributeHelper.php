<?php

namespace Drupal\acumatica\Acumatica\Model;

/**
 * Wrapper of helper methods for handling Acumatica attributes.
 */
class AttributeHelper {

  /**
   * Returns the value of the given attribute for the given object.
   *
   * @param string $attribute_id
   *   The ID of the attribute to get the value for.
   * @param object $object
   *   The JSON serialized object.
   *
   * @return mixed|null
   *   The value of the attribute, or NULL if the value is not set.
   *
   * @throws \RuntimeException
   *   When the object does not have any attributes, does not have the requested
   *   attribute, or does not have the value field defined.
   */
  public static function getValue(string $attribute_id, \stdClass $object) {
    if (!isset($object->Attributes)) {
      throw new \RuntimeException(
        'The given object does not have any attributes.'
      );
    }

    foreach ($object->Attributes as $attribute) {
      // The attribute ID cannot ever be NULL.
      if (!isset($attribute->AttributeID->value)) {
        throw new \RuntimeException(
          "Attribute ID not defined on one or more of the given entity object's attributes."
        );
      }

      if ($attribute->AttributeID->value !== $attribute_id) {
        continue;
      }

      // So far it seems that if no value is set for an attribute, the `value`
      // property on the `Value` field of the attribute object will not be set
      // i.e. the `Value` field object will be an empty object. This makes it
      // impossible to differentiate between whether the value is not defined in
      // the object because it is `NULL`, or because the API call used to fetch
      // the object did not properly include the required OData select/expand
      // conditions. In the latter case we would want to raise an error. The
      // only thing we can do is check that the `Value` field exists and return
      // `NULL` if it does not have a property value. The caller must ensure
      // that the object has the attributes properly expanded.
      if (!property_exists($attribute, 'Value')) {
        throw new \RuntimeException(
          "Value field not defined on one or more of the given entity object's attributes.",
        );
      }

      return $attribute->Value->value ?? NULL;
    }

    throw new \RuntimeException(sprintf(
      'The requested attribute "%s" is not defined on the given object\'s class.',
      $attribute_id
    ));
  }

}
