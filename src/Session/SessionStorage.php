<?php

declare(strict_types=1);

namespace Drupal\acumatica\Session;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;

use KrystalCode\Acumatica\Api\Session\SessionInterface;
use KrystalCode\Acumatica\Api\Session\SessionStorageInterface;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * An Acumatica session storage implementation using a key/value store.
 *
 * This currently works only with access token sessions. It can be made to work
 * with any sessions e.g. cookie session - given the right discriminator
 * resolver - but it will require the following changes.
 * - Define a `hasExpired` method on
 *   `\KrystalCode\Acumatica\Api\Session\SessionInterface` and call that here
 *   instead of fetching the access token and then calling its `hasExpired`
 *   method.
 * - Provide a way to determine the ignored attributes when serializing
 *   depending on the session type instead of hard-coding them or setting them
 *   for all session types whether they exist or not. See
 *   `Drupal\acumatica\Session\SessionStorage::serialize()`.
 */
class SessionStorage implements SessionStorageInterface {

  /**
   * The default key value collection used for storing the sessions.
   *
   * @var string
   */
  public const KEY_VALUE_COLLECTION = 'acumatica.sessions';

  /**
   * The key value store.
   */
  protected KeyValueStoreInterface $store;

  /**
   * Static cache.
   */
  protected array $cache = [];

  /**
   * The serializer.
   */
  protected SerializerInterface $serializer;

  /**
   * Constructs a new SessionStorage object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key value store to use.
   * @param string $discriminatorResolverClass
   *   The class of the discriminator resolver to use for the session
   *   serializer/normalizer.
   */
  public function __construct(
    protected KeyValueFactoryInterface $keyValueFactory,
    protected string $discriminatorResolverClass
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function set(SessionInterface $session): void {
    $id = $session->getId();
    $this->cache[$id] = $session;
    $this->store()->set($id, $this->serialize($this->cache[$id]));
  }

  /**
   * {@inheritdoc}
   */
  public function get(
    string $id = SessionInterface::SESSION_ID_DEFAULT
  ): ?SessionInterface {
    $session = $this->cache[$id] ?? NULL;

    if ($session === NULL) {
      $this->cache[$id] = $this->getFromStore($id);
      $session = $this->cache[$id];
    }

    // The interface requires us to not return expired sessions. The
    // `hasExpired` method always calculate its return value i.e. the result is
    // not cached.
    if ($session === NULL || $session->getAccessToken()->hasExpired()) {
      return NULL;
    }

    return $session;
  }

  /**
   * {@inheritdoc}
   */
  public function exists(
    string $id = SessionInterface::SESSION_ID_DEFAULT
  ): bool {
    // We don't check existing in the cache because we load sessions as
    // requested. There may therefore exist sessions in the store that do not
    // yet exist in the cache.
    //
    // Also, the interface requires us to not return expired sessions. We
    // therefore cannot just check whether the session exists in the store; we
    // have to load it and check whether it has expired.
    $session = $this->getFromStore($id);
    // The `hasExpired` method always calculate its return value i.e. the result
    // is not cached.
    if ($session === NULL || $session->getAccessToken()->hasExpired()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(
    string $id = SessionInterface::SESSION_ID_DEFAULT
  ): void {
    // From the interface it is not clear whether key value store
    // implementations would fail if they would be instructed to delete a
    // non-existing entry. We therefore check if the session exists first before
    // requesting to delete it.
    if ($this->store()->has($id)) {
      $this->store()->delete($id);
    }
    $this->cache[$id] = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCount(): int {
    // We don't get the count from the cache because we load sessions as
    // requested. There may therefore exist sessions in the store that do not
    // yet exist in the cache.
    //
    // Also, the interface requires us to not count expired sessions. We
    // therefore have to filter out expired sessions.
    return count(array_filter(
      $this->store()->getAll(),
      function ($session) {
        // The `hasExpired` method always calculate its return value i.e. the
        // result is not cached.
        return !$session->getAccessToken()->hasExpired();
      }
    ));
  }

  /**
   * Loads and returns a session for the given ID directly from the store.
   *
   * @param string $id
   *   The session ID.
   *
   * @return \Drupal\acumatica\Session\SessionInterface|null
   *   The session, or `NULL` if there is no session for the given ID.
   */
  protected function getFromStore(string $id): ?SessionInterface {
    $session = $this->store()->get($id);
    if ($session !== NULL) {
      $session = $this->deserialize($session);
    }

    return $session;
  }

  /**
   * Instantiates and returns the key value store.
   *
   * @return \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   *   The key value store.
   */
  protected function store(): KeyValueStoreInterface {
    if (isset($this->store)) {
      return $this->store;
    }

    $this->store = $this->keyValueFactory->get(static::KEY_VALUE_COLLECTION);
    return $this->store;
  }

  /**
   * Instantiates and returns the session serializer.
   *
   * @return \Symfony\Component\Serializer\SerializerInterface
   *   The serializer.
   */
  protected function serializer(): SerializerInterface {
    if (isset($this->serializer)) {
      return $this->serializer;
    }

    $this->serializer = new Serializer(
      [
        new ObjectNormalizer(
          NULL,
          NULL,
          NULL,
          NULL,
          new $this->discriminatorResolverClass()
        ),
      ],
      [new JsonEncoder()]
    );

    return $this->serializer;
  }

  /**
   * Serializes the session object preparing it for storage.
   *
   * @param \KrystalCode\Acumatica\Api\Session\SessionInterface $session
   *   The session to serialize.
   *
   * @return string
   *   The serialized JSON string.
   */
  protected function serialize(SessionInterface $session): string {
    return $this->serializer()->serialize(
      $session,
      JsonEncoder::FORMAT,
      [
        AbstractNormalizer::IGNORED_ATTRIBUTES => [
          // We don't store the `expired` flag. It not be valid when the session
          // is retrieved after expiration.
          'expired',
          // We don't store the `timeNow` timestamp because it is a static
          // property i.e. shared by all access tokens with the idea that it
          // always holds the request time. However, if we load it then from an
          // old session it results in new tokens created to believe that the
          // expiration time returned by the authorization server (`expires_in`)
          // is on top of the old session's start time thus calculating a wrong
          // expiration time.
          'timeNow',
        ],
      ]
    );
  }

  /**
   * Deserializes a JSON string back into a session object.
   *
   * @param string $serialized_session
   *   The serialized JSON string.
   *
   * @return \KrystalCode\Acumatica\Api\Session\SessionInterface
   *   The deserialized session.
   */
  protected function deserialize(string $serialized_session): SessionInterface {
    return $this->serializer()->deserialize(
      $serialized_session,
      SessionInterface::class,
      JsonEncoder::FORMAT
    );
  }

}
