<?php

declare(strict_types=1);

namespace Drupal\acumatica\Session\OAuth2;

use Drupal\Component\Datetime\TimeInterface;

use KrystalCode\Acumatica\Api\Configuration;
use KrystalCode\Acumatica\Api\Session\AccessTokenSession;
use KrystalCode\Acumatica\Api\Exception\Connection as ConnectionException;
use KrystalCode\Acumatica\Api\Session\SessionInterface;
use KrystalCode\Acumatica\Api\Session\SessionManagerInterface;
use KrystalCode\Acumatica\Api\Session\SessionStorageInterface;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Provides a session manager that uses Resource Owner OAuth2 flow.
 *
 * This session manager supports maintaining one session with one access token
 * requested for the `api` scope only i.e. no `api:concurrent` scope. The
 * session and its access token can be reused as many times as needed by
 * multiple processes e.g. queues, cron, web requests etc. until it
 * expires. When it expires - or when it has less time to live than what is
 * defined in configuration - a new access token is requested. Acumatica may
 * consider the new access token as a new session, but for our purposes we
 * consider this as refreshing and continuing the same global session.
 *
 * It seems that Acumatica issues access tokens for 1 hour (at least on the
 * account tested with Resource Owner OAuth2 flow). With this mechanism we are
 * sure to use only 1 session per hour ensuring that we do not reach the session
 * limits even if we make use of the same access token in multiple processes
 * potentially running in parallel.
 *
 * A separate session manager could be provided for maintaining multiple
 * concurrent sessions.
 *
 * Hitting API rate limits that restrict the number of concurrent or queued
 * requests that can be made can be avoided by spacing out requests by the same
 * process (e.g. queue run or drush command). The Entity Synchronization module
 * that this module integrates with provides options that can be used to
 * configure such behavior.
 */
class PasswordSessionManager implements SessionManagerInterface {

  /**
   * The maximum session duration that this session manager supports.
   */
  public const MAXIMUM_SESSION_DURATION = 3600;

  /**
   * Constructs a new PasswordSessionManager.
   *
   * @param \KrystalCode\Acumatica\Api\Session\SessionStorageInterface $sessionStorage
   *   The session storage.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Drupal's system time service.
   * @param \KrystalCode\Acumatica\Api\Configuration $config
   *   The API configuration.
   * @param array $options
   *   An assocative array of options. For supported options see
   *   `\KrystalCode\Acumatica\Api\Session\SessionManagerInterface::connect()`.
   *   No additional options added here.
   */
  public function __construct(
    protected SessionStorageInterface $sessionStorage,
    protected TimeInterface $time,
    protected Configuration $config,
    protected array $options = []
  ) {
    $this->options = array_merge(
      $this->getDefaultOptions(),
      $this->options
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(Configuration $config): void {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): Configuration {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions(array $options): void {
    $this->options = array_merge(
      $this->getDefaultOptions(),
      $options
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(): array {
    return $this->options;
  }

  /**
   * {@inheritdoc}
   *
   * The known maximum session duration is 1 hour i.e. Acumatica seems to set
   * the expiration time to 1 hour. It is unknown whether this is configurable
   * per Acumatica instance. If it turns out that this might be different for
   * different Acumatica instances, code changes will be required to account for
   * that.
   */
  public function connect(): object {
    $duration = $this->options['session_duration'];
    $this->validateDuration($duration);

    $token = NULL;

    // We always use one global session with the default session ID in this
    // session manager. If we don't already have a global session, create one.
    $session = $this->sessionStorage->get();
    if ($session === NULL) {
      [$session, $token] = $this->createOrUpdateSession();
    }
    else {
      $token = $session->getAccessToken();
    }

    // Check whether the session needs refreshing due to having expired or due
    // to not having sufficient time left.
    if ($token->hasExpired()) {
      [$session, $token] = $this->createOrUpdateSession($session);
    }

    $needs_refresh = TRUE;
    if ($duration === NULL) {
      $needs_refresh = FALSE;
    }
    elseif ($this->hasTimeToExpire($token, $duration)) {
      $needs_refresh = FALSE;
    }

    // Refresh token if needed.
    if ($needs_refresh) {
      [$session, $token] = $this->createOrUpdateSession($session);
    }

    // Return the client configuration with the access token for this
    // session. It will be used by the client discovery for creating the API
    // client.
    $client_config = $this->config->toClientConfiguration();
    $client_config->setAccessToken($token->getToken());
    return $client_config;
  }

  /**
   * Creates or updates an existing session by requesting a new access token.
   *
   * @param \KrystalCode\Acumatica\Api\Session\SessionInterface $session
   *   The session to update, or NULL to create a new one.
   *
   * @return array
   *   An array containing the session as its first and the access token as its
   *   second element.
   */
  protected function createOrUpdateSession(
    SessionInterface $session = NULL
  ): array {
    $token = $this->authenticate();

    if ($session === NULL) {
      $session = new AccessTokenSession($token);
    }
    else {
      $session->setAccessToken($token);
    }

    $this->sessionStorage->set($session);
    return [$session, $token];
  }

  /**
   * It authenticates to Acumatica and generates a new access token.
   *
   * @return \League\OAuth2\Client\Token\AccessTokenInterface
   *   The generated access token.
   */
  protected function authenticate(): AccessTokenInterface {
    $authentication = $this->config->getAuthentication();
    $this->validateAuthentication($authentication);

    $provider = new GenericProvider([
      'clientId' => $authentication['client_id'],
      'clientSecret' => $authentication['client_secret'],
      'urlAuthorize' => $authentication['authorize_url'],
      'urlAccessToken' => $authentication['access_token_url'],
      'urlResourceOwnerDetails' => $authentication['resource_owner_details_url'],
    ]);
    return $provider->getAccessToken(
      'password',
      [
        'username' => $authentication['username'],
        'password' => $authentication['password'],
        'scope' => $authentication['scope'] ?? 'api',
      ]
    );
  }

  /**
   * Returns whether the token has more than the given interval till expiration.
   *
   * @param \League\OAuth2\Client\Token\AccessTokenInterface $token
   *   The access token.
   * @param int $interval
   *   The interval to check, in seconds.
   */
  protected function hasTimeToExpire(
    AccessTokenInterface $token,
    int $interval
  ): bool {
    return $token->getExpires() > ($this->time->getCurrentTime() + $interval);
  }

  /**
   * Validates that the given duration is within supported limits.
   *
   * @param int|null $duration
   *   The duration to validate.
   *
   * @throws \KrystalCode\Acumatica\Api\Exception\Connection
   *   When the given duration exceeds the maximum supported by this session
   *   manager.
   */
  protected function validateDuration(int $duration = NULL) {
    if ($duration === NULL) {
      return;
    }
    if ($duration <= static::MAXIMUM_SESSION_DURATION) {
      return;
    }

    throw new ConnectionException(sprintf(
      '"%s" exceeds the maximum supported session duration that is "%s" seconds',
      $duration,
      static::MAXIMUM_SESSION_DURATION
    ));
  }

  /**
   * Validates the given authentication details.
   *
   * @param array $authentication
   *   The authentication details array.
   *
   * @throws \InvalidArgumentException
   *   When the requested authentication type/flow is invalid, or when one or
   *   more required properties are missing from the given authentication
   *   details.
   */
  protected function validateAuthentication(
    array $authentication
  ): void {
    if (($authentication['type'] ?? NULL) !== 'oauth2') {
      throw new \InvalidArgumentException(sprintf(
        '"%s" only supports the "oauth2" authentication type.',
        __CLASS__
      ));
    }
    if (($authentication['flow'] ?? NULL) !== 'password') {
      throw new \InvalidArgumentException(sprintf(
        '"%s" only supports the "password" OAuth2 authentication flow.',
        __CLASS__
      ));
    }

    $required_properties = [
      'client_id',
      'client_secret',
      'authorize_url',
      'access_token_url',
      'resource_owner_details_url',
      'username',
      'password',
    ];
    foreach ($required_properties as $property) {
      if (isset($authentication[$property])) {
        continue;
      }

      throw new \InvalidArgumentException(sprintf(
        '"%s" requires the "%s" authentication property.',
        __CLASS__,
        $property
      ));
    }
  }

  /**
   * Returns the default options.
   *
   * @return array
   *   An array containing the default options.
   */
  protected function getDefaultOptions() {
    return ['session_duration' => NULL];
  }

}
