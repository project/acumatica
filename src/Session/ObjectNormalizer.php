<?php

declare(strict_types=1);

namespace Drupal\acumatica\Session;

use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer as SymfonyObjectNormalizer;

/**
 * Normalizer for Acumatica session objects.
 *
 * The access token object requires specific options in its constructor when
 * instantiating. Context builders were introduced in serializer v6.1 while
 * Drupal currently requires v4.4+. We therefore provide a custom normalizer
 * that can properly instantiate the access token object contained as a property
 * in the session object.
 */
class ObjectNormalizer extends SymfonyObjectNormalizer {

  /**
   * {@inheritdoc}
   */
  protected function instantiateObject(
    array &$data,
    $class,
    array &$context,
    \ReflectionClass $reflection_class,
    $allowed_attributes,
    string $format = NULL
  ) {
    if ($class !== AccessToken::class) {
      return parent::instantiateObject(
        $data,
        $class,
        $context,
        $reflection_class,
        $allowed_attributes,
        $format
      );
    }

    return new $class([
      'access_token' => $data['token'],
      'resource_owner_id' => $data['resourceOwnerId'],
      'refresh_token' => $data['refreshToken'],
      'expires' => $data['expires'],
      'token_type' => $data['values']['token_type'],
      'scope' => $data['values']['scope'],
    ]);
  }

}
