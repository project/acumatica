<?php

namespace Drupal\acumatica\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that converts an array to an object.
 *
 * It can be used, for example, to convert a Drupal field value array to an
 * object that will be the value of a parent object field. That is the case, for
 * example, for address field.
 *
 * The object would commonly be a model provided by the OpenApi-generated
 * Acumatica API client, and it must accept the property values as an array in
 * its constructor - which is the case for the above mentioned models.
 *
 * Currently only exports are supported, support for imports should be added
 * here as well.
 *
 * Supported configuration:
 * - object_class: (string, required) The class of the object to create.
 *
 * @EntitySyncFieldTransformer(
 *   id = "acumatica_array_to_object"
 * )
 */
class ArrayToObject extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'object_class',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $array,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($array === NULL) {
      return NULL;
    }
    if (!is_array($array)) {
      throw new \RuntimeException(sprintf(
        'Expecting array, %s given.',
        gettype($array)
      ));
    }

    $class_name = $this->configuration['object_class'];
    return new $class_name($array);
  }

}
