<?php

namespace Drupal\acumatica\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that imports/exports the value of address fields.
 *
 * It converts the properties of Drupal `address` fields provided by the
 * `address` module to an array containing the properties as expected by
 * Acumatica address objects.
 *
 * Currently only exports are supported, support for imports should be added
 * here as well.
 *
 * The property mapping is as follows (Drupal/Acumatica):
 * - country_code/country
 * - administrative_area/state
 * - locality/city
 * - postal_code/postal_code
 * - address_line1/address_line1
 * - address_line2/address_line2
 *
 * The following properties are ignored in exports because they are not
 * supported by Acumatica address fields:
 * - dependent_locality
 * - sorting_code
 * - organization
 * - given_name
 * - additional_name
 * - family_name
 *
 * To further convert the array into an address object, for example, if required
 * to be sent as an object referenced in another object's field e.g. a
 * contact's address, further transform the array in a pipeline with the
 * `array_to_object` field transformer.
 *
 * @EntitySyncFieldTransformer(
 *   id = "acumatica_address"
 * )
 */
class Address extends PluginBase {

  /**
   * {@inheritdoc}
   *
   * The address must be passed as an array containing the properties, as
   * returned by the field API's `getValue` method.
   */
  protected function transformExportedValue(
    $address,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($address === NULL) {
      return NULL;
    }

    return [
      'country' => $this->transformProperty($address, 'country_code'),
      'state' => $this->transformProperty($address, 'administrative_area'),
      'city' => $this->transformProperty($address, 'locality'),
      'postal_code' => $this->transformProperty($address, 'postal_code'),
      'address_line1' => $this->transformProperty($address, 'address_line1'),
      'address_line2' => $this->transformProperty($address, 'address_line2'),
    ];
  }

  /**
   * Transforms a Drupal address property to an Acumatica field property.
   *
   * In addition to the standard conversion to a `value` array, it converts
   * postal codes (currently for US and CA) to pass Acumatica's postal code
   * validation.
   *
   * @param array $address
   *   The address field property array.
   * @param string $property
   *   The property to transform.
   *
   * @return array
   *   The property transformed into an Acumatica property array.
   */
  protected function transformProperty(
    array $address,
    string $property
  ) {
    if ($address[$property] === NULL || $address[$property] === '') {
      return NULL;
    }

    if ($property === 'postal_code') {
      return ['value' => $this->transformPostalCode($address, $property)];
    }

    return ['value' => $address[$property]];
  }

  /**
   * Transforms the Drupal address postal code to an Acumatica postal code.
   *
   * @param array $address
   *   The address field property array.
   * @param string $property
   *   The property to transform. It should always be `postal_code`.
   *
   * @return string
   *   The transformed postal code string.
   */
  protected function transformPostalCode(
    array $address,
    string $property
  ): string {
    switch ($address['country_code']) {
      // For US, Drupal allows space as a separator for 9-digit postal
      // codes. Acumatica, only allows `-` as a separator.
      case 'US':
        return str_replace(' ', '-', $address[$property]);

      // For CA, Drupal allows lowercase and no space. Acumatica requires all
      // characters to be uppercase, and a space to separate the two 3-character
      // groups.
      case 'CA':
        $postal_code = strtoupper($address[$property]);
        // Insert a space, if missing.
        if (strlen($postal_code) === 6) {
          $postal_code = substr_replace(
            $postal_code,
            ' ',
            3,
            0
          );
        }
        return $postal_code;

      default:
        return $address[$property];
    }
  }

}
