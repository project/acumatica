<?php

namespace Drupal\acumatica\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that imports/exports the value of timestamp fields.
 *
 * It converts a Unix timestamp to ISO8601 format in UTC as expected by
 * Acumatica.
 *
 * Currently only exports are supported, support for imports should be added
 * here as well.
 *
 * @EntitySyncFieldTransformer(
 *   id = "acumatica_timestamp"
 * )
 */
class Timestamp extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $timestamp,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($timestamp === NULL) {
      return NULL;
    }

    $datetime = new \DateTime('now', new \DateTimeZone('UTC'));
    $datetime->setTimestamp($timestamp);

    return $datetime->format('Y-m-d\TH:i:s') . ".000";
  }

}
