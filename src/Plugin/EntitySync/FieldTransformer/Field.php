<?php

namespace Drupal\acumatica\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that imports/exports the value of an Acumatica field.
 *
 * Extracts the main property value of a field attached to an Acumatica
 * object.
 *
 * Acumatica fields are similar to Drupal in that they contain their values in
 * properties. This plugin currently works with simple fields that have one main
 * property. It extracts and returns that main property, which most commonly is
 * name `value`. We haven't worked yet with complex fields to understand how
 * those should be handled. For simple fields we provide a configuration option
 * for changing the main property.
 *
 * Supported configuration:
 * - field_name: (string, optional) The name of the field. When importing, if
 *   the transformer is the only one or the first in a top-level pipeline, the
 *   object of the field defined in the field mapping is passed as the initial
 *   value to the transformer. If however the transformer is in a pipeline we
 *   may have the parent object and we need to get the field object before being
 *   able to extract its value. Relates to imports only.
 * - property_name: (string, optional, defaults to `value`) The name of the main
 *   property of the Acumatica field being imported/exported.
 *
 * Note that following field transformer conventions this transformer will
 * return `NULL` if the value does not exist, and not `['value' => NULL]`. This
 * is so that other features provided by the base field transformer do not break
 * when used individually or in a pipeline i.e. the `null_value` setting is
 * respected and validation can be aware of whether the value is `NULL` e.g. use
 * a `NotNull` validation constraint.
 *
 * This means though that the final transformed value for `NULL` will be just
 * that and not `['value' => NULL]`, which is the value generally expected by
 * Acumatica on all fields. The recommendation is to use the following
 * configuration either on this plugin if used individually or on the pipeline
 * plugin if used as part of a pipeline:
 * - If the result should never be `NULL`, use a `NotNull` constraint.
 * - If the result could be `NULL`, use the `null_value` configuration property
 *   to set the final value to `['value' => NULL]`.
 *
 * @EntitySyncFieldTransformer(
 *   id = "acumatica_field"
 * )
 */
class Field extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'property_name' => 'value',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($value === NULL) {
      return NULL;
    }

    return [$this->configuration['property_name'] => $value];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \RuntimeException
   *   When the requested field does not exist on the entity object, or if the
   *   requested property does not exist on the field object.
   */
  protected function transformImportedValue(
    $object,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    if ($object === NULL) {
      return NULL;
    }

    $field = $this->getFieldFromObject($object);
    if ($field === NULL) {
      return NULL;
    }

    if (!property_exists($field, $this->configuration['property_name'])) {
      throw new \RuntimeException(sprintf(
        'Field "%s" of the given object does not have the "%s" property.',
        $this->configuration['field_name'],
        $this->configuration['property_name']
      ));
    }

    return $field->{$this->configuration['property_name']} ?? NULL;
  }

  /**
   * Returns the field object for the given object, according to configuration.
   *
   * As discussed in the documentation for the `field_name` configuration
   * option, if that option is set we assume that have an entity object and
   * extract the field object from it. If the configuration option is not set,
   * we assume that we already have the field object handed over to the
   * transformer at the begining of the process.
   *
   * @param object $object
   *   The object being transformed.
   *
   * @return object
   *   The field object, if found.
   *
   * @throws \RuntimeException
   *   When the requested field does not exist on the object.
   */
  protected function getFieldFromObject(\stdClass $object) {
    if (!isset($this->configuration['field_name'])) {
      return $object;
    }

    if (!property_exists($object, $this->configuration['field_name'])) {
      throw new \RuntimeException(sprintf(
        'Field "%s" does not exist on the given object.',
        $this->configuration['field_name']
      ));
    }

    return $object->{$this->configuration['field_name']};
  }

}
