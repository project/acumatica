<?php

namespace Drupal\acumatica\Plugin\EntitySync\FieldTransformer;

use Drupal\acumatica\Acumatica\Model\AttributeHelper;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that imports the value of an Acumatica attribute.
 *
 * So far, simple attributes are supported which have the value stored in a
 * field called `Value` with one main property called `value`. More complex
 * attributes with multiple properties - if they exist - could be supported here
 * in the future.
 *
 * @EntitySyncFieldTransformer(
 *   id = "acumatica_attribute"
 * )
 */
class Attribute extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return ['value' => $value];
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    // All the logic is on the helper method for reusability. This method would
    // raise errors if the attribute does not exist on the object. So far it
    // seems that if the attribute is defined on the object's class, the
    // attribute will exist on the object (if it is properly requested in the
    // API call that attributes are expanded) but its Value field will be
    // empty. Thus, we want to let the errors be raised as it would be an error
    // with the API calls not requesting the attributes to be expanded.
    return AttributeHelper::getValue(
      $field_info['remote_name'],
      $remote_entity
    );
  }

}
