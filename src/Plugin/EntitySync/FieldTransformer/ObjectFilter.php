<?php

namespace Drupal\acumatica\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that filters an array of objects based on a field value.
 *
 * Currently only imports are supported. The transformer receives an array of
 * Acumatica objects as the initial value and it returns an array containing
 * only the objects that the configured field has the configured value.
 *
 * If the field or the field's main property is not defined on at least one
 * object, an error is raised. This behavior could become configurable in the
 * future.
 *
 * Currently supports only filter on a field's main property. Filtering based on
 * complex fields with multiple properties could be added in the future.
 *
 * Supported configuration:
 * - field_name: (string, required) The name of the field to check the value
 *   for.
 * - property_name: (string, optional, defaults to `value`) The name of the main
 *   property of the field.
 * - property_value: (string, required) The name of the main
 *   property of the field.
 *
 * @EntitySyncFieldTransformer(
 *   id = "acumatica_object_filter"
 * )
 */
class ObjectFilter extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'property_name' => 'value',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'field_name',
      'property_value',
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \RuntimeException
   *   When the requested field does not exist on the entity object, or if the
   *   requested property does not exist on the field object.
   */
  protected function transformImportedValue(
    $array,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    if ($array === NULL) {
      return NULL;
    }
    if (!is_array($array)) {
      throw new \InvalidArgumentException(sprintf(
        'An array of objects is expected to filter, %s given.',
        gettype($array)
      ));
    }

    return array_filter(
      $array,
      function ($object) {
        if (!property_exists($object, $this->configuration['field_name'])) {
          throw new \RuntimeException(sprintf(
            'Field "%s" does not exist on the given object.',
            $this->configuration['field_name']
          ));
        }

        $field = $object->{$this->configuration['field_name']};
        if (!property_exists($field, $this->configuration['property_name'])) {
          throw new \RuntimeException(sprintf(
            'Field "%s" of the given object does not have the "%s" property.',
            $this->configuration['field_name'],
            $this->configuration['property_name']
          ));
        }

        if ($field->value === $this->configuration['property_value']) {
          return TRUE;
        }

        return FALSE;
      }
    );
  }

}
