<?php

declare(strict_types=1);

namespace Drupal\acumatica\Plugin\EntitySync\OperationConfigurator;

use Drupal\acumatica\EntitySync\OperationConfigurator\PluginBase;

/**
 * Plugin for operations that relate to a list of entities.
 *
 * That is, for operations synchronizing lists of entities between Drupal and
 * Acumatica. Currently only imports are supported. Support for exports should
 * be added in this plugin.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "acumatica_entity_list",
 *   label = @Translation("Acumatica entity list import"),
 *   description = @Translation(
 *     "Use 'Acumatica entity list import' types to import a list of Acumatica objects into Drupal entities."
 *   ),
 *   action_types = {
 *     "import_list",
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 */
class EntityList extends PluginBase {

  /**
   * The ID of the operation runner service.
   */
  const RUNNER_SERVICE_ID = 'acumatica.entity_sync_runner.entity_list';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'action_type' => NULL,
      'local_entity' => [],
      'remote_resource' => [
        'provider_id' => 'acumatica',
        'id_field' => 'id',
        'client' => [
          'type' => 'factory',
          'service' => 'acumatica.entity_sync_client.object_factory',
          'parameters' => [],
        ],
      ],
      'operations' => [],
      'field_mapping' => [],
      'ui' => [
        'disallowed_transitions' => [
          'fail',
          'complete',
        ],
        'uneditable_states' => [
          'cancelled',
          'running',
          'completed',
          'failed',
        ],
      ],
    ] + parent::defaultConfiguration();
  }

}
