<?php

declare(strict_types=1);

namespace Drupal\acumatica\Plugin\EntitySync\OperationConfigurator;

use Drupal\acumatica\EntitySync\OperationConfigurator\PluginBase;

/**
 * Plugin for operations that import a list of entities using OData feeds.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "acumatica_entity_feed",
 *   label = @Translation("Acumatica entity feed import"),
 *   description = @Translation(
 *     "Use 'Acumatica entity feed import' types to import a feed of Acumatica objects into Drupal entities."
 *   ),
 *   action_types = {
 *     "import_list",
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 */
class EntityFeed extends PluginBase {

  /**
   * The ID of the operation runner service.
   */
  const RUNNER_SERVICE_ID = 'acumatica.entity_sync_runner.entity_list';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'action_type' => NULL,
      'local_entity' => [],
      'remote_resource' => [
        'id_field' => 'id',
        'client' => [
          'type' => 'factory',
          'service' => 'acumatica.entity_sync_client.feed_factory',
          'parameters' => [],
        ],
      ],
      'operations' => [],
      'field_mapping' => [],
      'ui' => [
        'disallowed_transitions' => [
          'fail',
          'complete',
        ],
        'uneditable_states' => [
          'cancelled',
          'running',
          'completed',
          'failed',
        ],
      ],
    ] + parent::defaultConfiguration();
  }

}
