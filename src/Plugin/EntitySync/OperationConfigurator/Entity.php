<?php

declare(strict_types=1);

namespace Drupal\acumatica\Plugin\EntitySync\OperationConfigurator;

use Drupal\acumatica\EntitySync\OperationConfigurator\PluginBase;
use Drupal\entity_sync\OperationConfigurator\FieldTrait\EntityTrait;

/**
 * Plugin for operations that relate to individual entities.
 *
 * Currently only exports are supported. Support for imports should also be
 * added in this plugin.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "acumatica_entity",
 *   label = @Translation("Acumatica entity export"),
 *   description = @Translation(
 *     "Use 'Acumatica entity export' types to export Drupal entities into Acumatica objects."
 *   ),
 *   action_types = {
 *     "export_entity",
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 */
class Entity extends PluginBase {

  use EntityTrait;

  /**
   * The ID of the operation runner service.
   */
  const RUNNER_SERVICE_ID = 'acumatica.entity_sync_runner.entity';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'action_type' => NULL,
      'local_entity' => [],
      'remote_resource' => [
        'provider_id' => 'acumatica',
        'client' => [
          'type' => 'factory',
          'service' => 'acumatica.entity_sync_client.object_factory',
          'parameters' => [],
        ],
      ],
      'operations' => [],
      'field_mapping' => [],
      'ui' => [
        'disallowed_transitions' => [
          'fail',
          'complete',
        ],
        'uneditable_states' => [
          'cancelled',
          'running',
          'completed',
          'failed',
        ],
      ],
      'plugin' => [
        // Each Acumatica object class has one or more primary keys. Objects can
        // be retrieved using these objects and the `getByKeys` API client
        // method.
        //
        // The mapping of the key fields to the corresponding Drupal fields can
        // be defined here as an associative array keyed by the machine name of
        // the Acumatica key field and containing the machine name of the
        // corresponding Drupal field.
        //
        // Currently, they are used by the export entity mapping subscriber to
        // detect existing unassociated Acumatica objects when exporting Drupal
        // entities in order to prevent duplicate objects in Acumatica. For that
        // feature to work, the following needs to be true:
        // - A field transformer must be defined in the field mapping for each
        //   combination of remote/local field machine names.
        // - The elements in the array must be in the order required to be
        //   passed in the URL when retrieving an object by keys.
        //
        // See \Drupal\acumatica\EventSubscriber\ExportLocalEntityMapping::buildEntityMapping().
        // See https://help.acumatica.com/(W(73))/Help?ScreenId=ShowWiki&pageid=52c97a83-1fa1-40e9-8219-52a89a91f2da.
        'key_field_mapping' => [],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function bundleFieldDefinitions() {
    $fields = parent::bundleFieldDefinitions();

    // Add the dynamic entity reference field.
    $fields += $this->buildEntityBundleFieldDefinitions();

    return $fields;
  }

}
