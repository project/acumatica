<?php

namespace Drupal\acumatica\Constant\Field;

/**
 * Holds IDs for entity fields.
 *
 * These are fields that are common to multiple entity types provided by this
 * module. Fields specific to an entity type should go to a separate class named
 * by the entity type and bundle as required in each case.
 */
class Entity {

  /**
   * The machine name for the ID field.
   */
  const ID = 'id';

  /**
   * The machine name for the UUID field.
   */
  const UUID = 'uuid';

  /**
   * The machine name for the created field.
   */
  const CREATED = 'created';

  /**
   * The machine name for the updated field.
   */
  const CHANGED = 'changed';

  /**
   * The machine name of the Acumatica ID sync field.
   *
   * Holds the ID of the linked object in Acumatica.
   */
  const ACUMATICA_ID = 'sync_acumatica_id';

  /**
   * The machine name of the code field.
   *
   * Holds a code that acts as a unique identifier other than their primary ID.
   * Used by ShipVia, Carrier and other objects.
   */
  const CODE = 'code';

  /**
   * The machine name of the description field.
   *
   * Holds a short description of the object.
   */
  const DESCRIPTION = 'description';

}
