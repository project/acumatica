<?php

namespace Drupal\acumatica\Constant\EntityType;

/**
 * Holds IDs for entity types provided by this module.
 */
class Id {

  /**
   * The ID of the Ship Via entity type.
   */
  const SHIP_VIA = 'acumatica_ship_via';

}
