<?php

declare(strict_types=1);

namespace Drupal\acumatica\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * The default implementation of the ConnectionType entity.
 *
 * phpcs:disable
 * @ConfigEntityType(
 *   id = "acumatica_connection_type",
 *   label = @Translation("Acumatica connection type"),
 *   label_collection = @Translation("Acumatica connection types"),
 *   label_singular = @Translation("Acumatica connection type"),
 *   label_plural = @Translation("Acumatica connection types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Acumatica connection type",
 *     plural = "@count Acumatica connection types",
 *   ),
 *   config_prefix = "connection_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "api_id",
 *     "api_version",
 *     "base_namespace",
 *     "authentication"
 *   }
 * )
 *
 * @I Add handler such as list builder, storage, forms
 *    type     : feature
 *    priority : normal
 *    labels   : ux
 * phpcs:enable
 */
class ConnectionType extends ConfigEntityBase implements
  ConnectionTypeInterface {

  /**
   * An ID i.e. machine name.
   *
   * @var string
   */
  protected $id;

  /**
   * A human-friendly label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description.
   *
   * @var string
   */
  protected $description;

  /**
   * The ID of the web service endpoint.
   *
   * @var string
   *
   * @see \Drupal\acumatica\Entity\ConnectionInterface::getApiId()
   */
  protected $api_id;

  /**
   * The version of the web service endpoint.
   *
   * @var string
   *
   * @see \Drupal\acumatica\Entity\ConnectionInterface::getApiVersion()
   */
  protected $api_version;

  /**
   * The base namespace of the API client.
   *
   * @var string
   *
   * @see \Drupal\acumatica\Entity\ConnectionInterface::getBaseNamespace()
   */
  protected $base_namespace;

  /**
   * The authentication settings for sessions .
   *
   * @var array
   */
  protected $authentication;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiId(): ?string {
    return $this->api_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiVersion(): ?string {
    return $this->api_version;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseNamespace(): ?string {
    return $this->base_namespace;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthentication(): ?array {
    return $this->authentication;
  }

}
