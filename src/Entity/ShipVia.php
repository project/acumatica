<?php

declare(strict_types=1);

namespace Drupal\acumatica\Entity;

use Drupal\acumatica\Constant\Field\Entity as EntityField;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The default implementation for the ShipVia content entity.
 *
 * Note that this entity might not need to be installed on many applications, if
 * it is not used in its integration. It can be uninstalled using the entity
 * definition update manager.
 *
 * @ContentEntityType(
 *   id = "acumatica_ship_via",
 *   label = @Translation("Acumatica ship via"),
 *   label_collection = @Translation("Acumatica ship via"),
 *   label_singular = @Translation("Acumatica ship via"),
 *   label_plural = @Translation("Acumatica ship via"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Acumatica ship via",
 *     plural = "@count Acumatica ship via",
 *   ),
 *   base_table = "acumatica_ship_via",
 *   admin_permission = "administer acumatica_ship_via",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "code",
 *   },
 * )
 */
class ShipVia extends ContentEntityBase implements ShipViaInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCode(): string {
    return $this->get(EntityField::CODE)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCode(string $code): ShipViaInterface {
    $this->set(EntityField::CODE, $code);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get(EntityField::DESCRIPTION)->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set(EntityField::DESCRIPTION, $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * More fields could be added in the future, or in custom modules. There's
   * more entities that would need to be provided for full integration
   * e.g. carrier, packages etc. and the scope of what will be provided by the
   * contrib module is not clear yet.
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type
  ) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Alterations on already defined fields.
    $fields[EntityField::ID]->setDescription(
      new TranslatableMarkup('The ID of the ship via record.')
    );
    $fields[EntityField::UUID]->setDescription(
      new TranslatableMarkup('The UUID of the ship via record.')
    );

    // ShipVia object details.
    $fields[EntityField::CODE] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Code'))
      ->setDescription(new TranslatableMarkup(
        'The code of the entity.'
      ))
      ->setRequired(TRUE)
      ->addConstraint('UniqueField')
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields[EntityField::DESCRIPTION] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Description'))
      ->setDescription(new TranslatableMarkup(
        'The description of the entity.'
      ))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Entity Synchronization sync field.
    // The object in Acumatica does not have a last modified field that could be
    // taken advantage for importing the objects only when changed, and for
    // breaking import/export loops. We therefore only store the ID.
    $fields[EntityField::ACUMATICA_ID] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Acumatica ID'))
      ->setDescription(new TranslatableMarkup(
        'The ID and the last modified time of the linked Acumatica record.',
      ))
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setSettings([
        'max_length' => 36,
        'is_ascii' => TRUE,
        'case_sensitive' => FALSE,
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // At last, timestamps.
    $fields[EntityField::CREATED] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup(
        'The time when the operation entity was created.'
      ))
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields[EntityField::CHANGED] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup(
        'The time when the operation entity was last edited.'
      ))
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    return $fields;
  }

}
