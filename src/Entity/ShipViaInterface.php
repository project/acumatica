<?php

namespace Drupal\acumatica\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for Acumatica Ship Via entities.
 */
interface ShipViaInterface extends
  ContentEntityInterface,
  EntityDescriptionInterface {

  /**
   * Returns the code.
   *
   * @return string
   *   The code of the Ship Via entity.
   */
  public function getCode(): string;

  /**
   * Set the code.
   *
   * @param string $code
   *   The code to set on the Ship Via entity.
   *
   * @return $this
   */
  public function setCode(string $code): ShipViaInterface;

}
