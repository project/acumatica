<?php

declare(strict_types=1);

namespace Drupal\acumatica\EventSubscriber;

use Drupal\entity_sync\Exception\EntityImportException;
use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Import\Event\RemoteEntityMappingEvent;
use Drupal\entity_sync\Import\ManagerInterface as EntityImportManagerInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Detects existing associations for variations imported for the first time.
 *
 * Applies to synchronizations that have one or more of the following labels:
 * - acumatica
 * - acumatica-mappings
 */
class VariationImportRemoteEntityMapping implements EventSubscriberInterface {

  /**
   * Constructs a new VariationImportRemoteEntityMapping object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      // Needs to run after the related subscriber provided by the `entity_sync`
      // contrib module. See
      // `\Drupal\entity_sync\EventSubscriber\DefaultImportRemoteEntityMapping`.
      Events::REMOTE_ENTITY_MAPPING => ['buildEntityMapping', -100],
    ];
    return $events;
  }

  /**
   * Detects whether a variation already exists before creating it.
   *
   * Most commonly, and by default, SKUs are unique i.e. there should always be
   * a unique variation for a given SKU. Variations may exist in Drupal before
   * they are added in Acumatica and before we connect local with remote
   * entities for the first time with the sync ID field. More existing
   * variations may be added in Acumatica even after initial deployment of the
   * integration. Existing variations will therefore be considered as new by the
   * default entity mapping provided by the Entity Synchronization module the
   * first time they are imported. We therefore need to avoid duplicate
   * variations from being created i.e. we detect variations with the same SKUs
   * and if found we set the entity mapping to update the existing variations
   * instead of creating new ones.
   *
   * The missing association is set by the field import manager.
   *
   * Note that if the sync ID field is set to be the SKU field, this should not
   * be necessary. The default mappings will detect the association based on the
   * SKU field in that case.
   *
   * @param \Drupal\entity_sync\Import\Event\RemoteEntityMappingEvent $event
   *   The local entity mapping event.
   */
  public function buildEntityMapping(RemoteEntityMappingEvent $event): void {
    if (!$this->applies($event)) {
      return;
    }

    $entity_mapping = $event->getEntityMapping();
    // If we already have the remote ID, we will have an update action set by
    // the default entity mapping.
    if ($entity_mapping['action'] !== EntityImportManagerInterface::ACTION_CREATE) {
      return;
    }

    // Otherwise, the variation either does not exist as a local entity yet, or
    // it exists but it has not been connected with the remote entity yet. Find
    // if we have a variation with the incoming SKU already and map it to that
    // if so. See
    // `\Drupal\entity_sync\EventSubscriber\DefaultImportRemoteEntityMapping::buildEntityMapping()`.
    $sku = $event->getRemoteEntity()->InventoryID->value ?? NULL;
    if (!$sku) {
      throw new EntityImportException(
        'The stock/non-stock item being imported does not have an inventory ID.'
      );
    }

    $variation_ids = $this->entityTypeManager
      ->getStorage($event->getSync()->getLocalEntitySettings()['type_id'])
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('sku', $sku)
      ->execute();
    if (!$variation_ids) {
      return;
    }
    if (count($variation_ids) > 1) {
      throw new EntityImportException(sprintf(
        'There is more than one variation with SKU "%s".',
        $sku
      ));
    }

    $entity_mapping['action'] = EntityImportManagerInterface::ACTION_UPDATE;
    $entity_mapping['id'] = current($variation_ids);

    $event->setEntityMapping($entity_mapping);
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * This subscriber is only relevant to Acumatica operations that use the
   * mapping logic provided by this module. That is, operations that one or more
   * of the following labels:
   * - acumatica
   * - acumatica-mappings
   *
   * @param \Drupal\entity_sync\Export\Event\RemoteEntityMappingEvent $event
   *   The remote entity mapping event.
   *
   * @return bool
   *   Whether the changes in this subscriber should be applied.
   */
  protected function applies(RemoteEntityMappingEvent $event): bool {
    $sync = $event->getSync();
    $has_labels = array_intersect(
      ['acumatica', 'acumatica-mappings'],
      $sync->getLabels() ?? []
    );
    if (!$has_labels) {
      return FALSE;
    }

    $entity_type_id = $sync->getLocalEntitySettings()['type_id'] ?? NULL;
    if ($entity_type_id !== 'commerce_product_variation') {
      return FALSE;
    }

    return TRUE;
  }

}
