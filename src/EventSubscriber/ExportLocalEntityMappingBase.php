<?php

declare(strict_types=1);

namespace Drupal\acumatica\EventSubscriber;

use Drupal\entity_sync\Client\ClientFactory;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\Exception\EntityExportException;
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Export\Event\LocalEntityMappingEvent;
use Drupal\entity_sync\Export\EntityManagerInterface as EntityExportManagerInterface;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

use KrystalCode\Acumatica\Api\Configuration;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides a base class for subscribers detecting existing remote entities.
 */
abstract class ExportLocalEntityMappingBase implements EventSubscriberInterface {

  /**
   * Constructs a new ExportLocalEntityMappingBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $fieldTransformerManager
   *   The field transformer plugin manager.
   * @param \Drupal\entity_sync\Client\ClientFactory $clientFactory
   *   The client factory.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected PluginManagerInterface $fieldTransformerManager,
    protected ClientFactory $clientFactory
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      // Needs to run after the related subscriber provided by the `entity_sync`
      // contrib module. See
      // `\Drupal\entity_sync\EventSubscriber\DefaultExportLocalEntityMapping`.
      Events::LOCAL_ENTITY_MAPPING => ['buildEntityMapping', -100],
    ];
    return $events;
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * @param \Drupal\entity_sync\Export\Event\LocalEntityMappingEvent $event
   *   The local entity mapping event.
   *
   * @return bool
   *   TRUE to apply the changes in this subscriber, FALSE to skip.
   */
  abstract protected function applies(LocalEntityMappingEvent $event): bool;

  /**
   * Searches for and returns the remote entity for the given local entity.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization or operation type configuration entity that defines
   *   the operation being run.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity being exported.
   * @param array $entity_mapping
   *   The current entity mapping.
   * @param \Drupal\entity_sync\Export\Event\LocalEntityMappingEvent $event
   *   The local entity mapping event.
   */
  abstract protected function findRemoteEntity(
    SyncInterface $sync,
    ContentEntityInterface $local_entity,
    array $entity_mapping,
    LocalEntityMappingEvent $event
  ): ?object;

  /**
   * Updates the default local entity mapping.
   *
   * There's a number of cases where objects may already exist in Acumatica but
   * are not associated yet with the corresponding Drupal entities. For example,
   * an import of products, customers or orders may have been done via CSV files
   * or from another ERP system. In such cases and when using the defaul entity
   * mapping based on the sync ID field that is provided by the Entity
   * Synchronization module, a new remote entity would be created in Acumatica.
   *
   * To prevent duplicates, before exporting a local entity that has not been
   * associated yet with a remote entity, we check whether there is already one
   * existing based on its keys or other fields.
   *
   * @param \Drupal\entity_sync\Export\Event\LocalEntityMappingEvent $event
   *   The local entity mapping event.
   */
  public function buildEntityMapping(LocalEntityMappingEvent $event): void {
    if (!$this->applies($event)) {
      return;
    }

    $entity_mapping = $event->getEntityMapping();
    // If we already have the remote ID, we will have an update action set by
    // the default entity mapping.
    if ($entity_mapping['action'] !== EntityExportManagerInterface::ACTION_CREATE) {
      return;
    }

    $sync = $event->getSync();
    $local_entity = $event->getLocalEntity();
    $remote_entity = $this->findRemoteEntity(
      $sync,
      $local_entity,
      $entity_mapping,
      $event
    );
    // If no remote entity was found, do nothing i.e. continue with the current
    // entity mapping and create a new remote entity.
    if ($remote_entity === NULL) {
      return;
    }

    $remote_id_field = $sync->getRemoteResourceSettings()['id_field'] ?? NULL;
    if ($remote_id_field === NULL) {
      throw new InvalidConfigurationException(
        'The remote ID field name is required for associating an existing remote object during an entity export.'
      );
    }
    $remote_entity_id = $remote_entity->{$remote_id_field} ?? NULL;
    if ($remote_entity_id === NULL) {
      throw new \RuntimeException(sprintf(
        'An existing remote object was found but it does not contain the expected ID field "%s".',
        $remote_id_field
      ));
    }

    // We save the remote entity ID in the sync ID field here. The terminate
    // event subscriber only saves it if it is a create action. Additionally, we
    // want to have the sync ID field available with the correct value in field
    // transformers and other event subscribers that may make use of it.
    $sync_id_field = $sync->getLocalEntitySettings()['remote_id_field'] ?? NULL;
    if ($sync_id_field === NULL) {
      throw new InvalidConfigurationException(
        'The sync ID field name is required for associating an existing remote object during an entity export.'
      );
    }
    $local_entity->set($sync_id_field, $remote_entity_id);
    $this->entityTypeManager
      ->getStorage($local_entity->getEntityTypeId())
      ->save($local_entity);

    $entity_mapping['action'] = EntityExportManagerInterface::ACTION_UPDATE;
    $entity_mapping['id'] = $remote_entity_id;

    $event->setEntityMapping($entity_mapping);
  }

  /**
   * Determines a remote field value using the corresponding field transformer.
   *
   * Even though most commonly searching for a remote entity is done using
   * simple string fields that would be stored as is in Drupal e.g. customer ID,
   * contact email, stock item inventory ID etc., we want to use the final
   * values as returned by the field transformers when doing the actual exports.
   *
   * There are two things that are different here compared to running the field
   * transformation during the actual export:
   * - The context is not available.
   * - The ID of the remote entity is not available.
   *
   * If these are used in the transformations of the fields used for searching
   * for the remote entity, you will want to review and customize the code as
   * needed in your event subscriber.
   *
   * @param string $remote_field
   *   The machine name of the remote field.
   * @param string $local_field
   *   The machine name of the local field.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization or operation type configuration entity that defines
   *   the operation being run.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity being exported.
   *
   * @return mixed
   *   The transformed value for the remote field.
   */
  protected function getRemoteFieldValueFromFieldTransformer(
    string $remote_field,
    string $local_field,
    SyncInterface $sync,
    ContentEntityInterface $local_entity
  ) {
    $matching_field_info = NULL;
    $field_mapping = $sync->getFieldMapping();
    foreach ($field_mapping as $field_info) {
      if (($field_info['machine_name'] ?? NULL) !== $local_field) {
        continue;
      }
      if (($field_info['remote_name'] ?? NULL) !== $remote_field) {
        continue;
      }

      $matching_field_info = $field_info;
    }

    // No field transformer found for the field.
    if ($matching_field_info === NULL) {
      throw new InvalidConfigurationException(
        'No field mapping found for the remote/local field combination.'
      );
    }

    try {
      return $this->fieldTransformerManager
        ->createInstance(
          $matching_field_info['export']['transformer']['id'],
          $matching_field_info['export']['transformer']['configuration'] ?? []
        )
        ->export(
          $local_entity,
          NULL,
          $matching_field_info,
          // Currently, we don't have the context available in the event.
          [],
          NULL,
          TRUE
        );
    }
    catch (\Throwable $throwable) {
      throw new EntityExportException(sprintf(
        'A "%s" exception was thrown while determining the values of the fields used for searching for existing object associations. The error message was: %s. The field mapping was: %s',
        get_class($throwable),
        $throwable->getMessage(),
        json_encode($matching_field_info)
      ));
    }
  }

  /**
   * Instantiates and returns the Acumatica API configuration.
   *
   * @param string $connection_type_id
   *   The ID of the connection type to use for building the Acumatica
   *   configuration.
   *
   * @return \KrystalCode\Acumatica\Api\Configuration
   *   The Acumatica API configuration.
   */
  protected function getAcumaticaConfiguration(
    string $connection_type_id
  ): Configuration {
    $connection_type = $this->entityTypeManager
      ->getStorage('acumatica_connection_type')
      ->load($connection_type_id);
    if ($connection_type === NULL) {
      throw new InvalidConfigurationException(sprintf(
        'Unknown Acumatica connection type "%s".',
        $connection_type_id
      ));
    }

    return new Configuration(
      $connection_type->getApiId(),
      $connection_type->getApiVersion(),
      $connection_type->getBaseNamespace(),
      $connection_type->getAuthentication()
    );
  }

}
