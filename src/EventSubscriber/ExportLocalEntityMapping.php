<?php

declare(strict_types=1);

namespace Drupal\acumatica\EventSubscriber;

use Drupal\entity_sync\Entity\OperationTypeInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\Export\Event\LocalEntityMappingEvent;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Checks for existing remote entities before creating new ones.
 *
 * Checks based on object keys. These must be registered in the operation
 * type plugin configuration using the `key_field_mapping` configuration
 * option.
 *
 * Applies to operation types that have one or more of the following labels:
 * - acumatica
 * - acumatica-mappings
 *
 * @see \Drupal\Drupal\acumatica\EventSubscriber\ExportLocalEntityMappingBase::buildEntityMapping()
 * @see \Drupal\acumatica\Plugin\EntitySync\OperationConfigurator::defaultConfiguration()
 */
class ExportLocalEntityMapping extends ExportLocalEntityMappingBase {

  /**
   * {@inheritdoc}
   *
   * This subscriber is only relevant to Acumatica operations that use the
   * mapping logic provided by this module. That is, operations that have one or
   * more of the following labels:
   * - acumatica
   * - acumatica-mappings
   */
  protected function applies(LocalEntityMappingEvent $event): bool {
    $has_labels = array_intersect(
      ['acumatica', 'acumatica-mappings'],
      $event->getSync()->getLabels() ?? []
    );
    if (!$has_labels) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * Loks for a remote entity based on keys using the `getByKeys` method
   * provided by the object client.
   */
  protected function findRemoteEntity(
    SyncInterface $sync,
    ContentEntityInterface $local_entity,
    array $entity_mapping,
    LocalEntityMappingEvent $event
  ): ?object {
    $key_values = $this->getKeyValues($sync, $local_entity);
    $acumatica_configuration = $this->getAcumaticaConfiguration(
      $entity_mapping['client']['parameters']['connection_type']
    );
    $client_namespace = $acumatica_configuration->getClientNamespace();
    $exception_class = $client_namespace . '\ApiException';

    // Acumatica does not return a `404` or empty response if no entity is
    // found. Instead, it returns a `500` server error response. This makes it
    // difficult to differentiate a no entity found response from an actual
    // server error. This is important because if we misinterpret an actual
    // server error as a no entity found response we could mistakenly create a
    // duplicate entity in Acumatica.
    // We therefore do a detailed check for the expected exception class, code
    // and message based on the project that this was tested on. If on other
    // projects the exception raised does not match, it will be thrown.
    // Developers should then customize the subscriber on their applications.
    try {
      $object = $this->clientFactory
        ->getByClientConfig($entity_mapping['client'])
        // The OpenAPI-generated clients concatenates keys provided as an array
        // using `,` before using them in the URL, while Acumatica actually
        // expects them separated by `/`. Do this here and provide all keys as
        // if it were one to avoid this problem.
        ->getByKeys([implode('/', $key_values)]);
    }
    catch (\Throwable $throwable) {
      if (!$throwable instanceof $exception_class) {
        throw $throwable;
      }

      if ($throwable->getCode() !== 500) {
        throw $throwable;
      }

      $response_object = $throwable->getResponseObject();
      $model_class = $client_namespace . '\Model\HttpError';
      if (!$response_object instanceof $model_class) {
        throw $throwable;
      }

      $expected_message = 'No entity satisfies the condition.';
      if ($response_object->getExceptionMessage() !== $expected_message) {
        throw $throwable;
      }

      // If after all these checks we have determined that this is indeed the
      // exception expected when the entity is not found, there must be no
      // matching object in Acumatica.
      return NULL;
    }

    // If no error was raised, we must have a matching entity.
    return $object;
  }

  /**
   * Returns the key values to use when for searching for the remote entity.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization or operation type configuration entity that defines
   *   the operation being run.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity being exported.
   *
   * @return array
   *   An array containing the key values. These should almost certainly be
   *   string values.
   */
  protected function getKeyValues(
    SyncInterface $sync,
    ContentEntityInterface $local_entity
  ): array {
    // Only applicable to operation types because we expect the key fields to be
    // defined in the plugin configuration.
    if (!$sync instanceof OperationTypeInterface) {
      throw new InvalidConfigurationException(
        'Only operation types are supported when detecting existing remote entity associations during local entity exports.'
      );
    }

    $key_mapping = $sync->getPluginConfiguration()['plugin']['key_field_mapping'] ?? [];
    if (!$key_mapping) {
      throw new InvalidConfigurationException(
        'The key field mapping must be provided.'
      );
    }

    $key_values = [];
    foreach ($key_mapping as $remote_key => $local_field) {
      $key_value = $this->getRemoteFieldValueFromFieldTransformer(
        $remote_key,
        $local_field,
        $sync,
        $local_entity
      );
      if (!$key_mapping) {
        throw new \RuntimeException(sprintf(
          'The key field "%s" cannot be empty.',
          $remote_key
        ));
      }

      // Field transformers are expected to return the value in an array element
      // keyed `value` because that's how Acumatica expects it.
      $key_values[] = $key_value['value'];
    }

    return $key_values;
  }

}
